import React from 'react';
import PropTypes from 'prop-types';
import { StatusBar, LogBox } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import Router from './routing/router.js';
import { withReduxProvider } from './redux/store';
import { primaryColor } from './utils/colors';

EStyleSheet.build({
  $primaryColor: primaryColor,
  $placeholderColor: 'rgba(0, 0, 0, 0.4)',
  $appMargin: 24,
  $gray: '#5D5D67',
  $lightBlue: '#E6ECFB',
});

const App = ({ isHeadless }) => {
  if (isHeadless) {
    return null;
  }

  LogBox.ignoreAllLogs();
  return (
    <SafeAreaProvider>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <Router />
    </SafeAreaProvider>
  );
};

App.propTypes = { isHeadless: PropTypes.bool };

export default withReduxProvider(App);
