import dayjs from 'dayjs';
import React from 'react';
import { View, Text } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import TransactionField from './transaction_field';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    padding: 12,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 8,
    marginBottom: 12,
  },
});

const Transaction = ({ transaction }) => {
  return (
    <View style={styles.container}>
      <TransactionField name="Time" value={dayjs(transaction.timeStamp).format('DD.MM.YYYY')} />
      <TransactionField name="From" value={transaction.from} />
      <TransactionField name="To" value={transaction.to} />
      <TransactionField name="Value" value={transaction.value} />
      <TransactionField name="Confirmation" value={transaction.confirmations} />
      <TransactionField name="Hash" value={transaction.hash} />
    </View>
  );
};

export default Transaction;
