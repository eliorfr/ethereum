import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';

import { OutlinedTextField } from 'rn-material-ui-textfield';
import { primaryColor } from '../utils/colors';

const styles = EStyleSheet.create({
  textField: {
    width: '100%',
  },
});

const TextField = React.forwardRef(({ ...props }, ref) => {
  return <OutlinedTextField ref={ref} containerStyle={styles.textField} tintColor={primaryColor} {...props} />;
});

export default TextField;
