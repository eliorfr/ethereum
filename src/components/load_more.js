import React from 'react';
import { ActivityIndicator, TouchableOpacity, Text, View } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';

const styles = EStylesheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    paddingVertical: 10,
    marginVertical: 10,
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    fontWeight: '500',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});

const LoadMore = ({ getData, loading }) => {
  return (
    <View style={styles.footer}>
      <TouchableOpacity activeOpacity={0.9} onPress={getData} style={styles.loadMoreBtn}>
        <Text style={styles.btnText}>Load More</Text>
        {loading ? <ActivityIndicator color="white" style={{ marginLeft: 8 }} /> : null}
      </TouchableOpacity>
    </View>
  );
};

export default LoadMore;
