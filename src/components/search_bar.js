import React from 'react';
import { TextInput, View, Text } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button } from 'react-native-paper';

import Icon from 'react-native-vector-icons/AntDesign';

const styles = EStylesheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(93, 93, 103, 0.06)',
    borderRadius: 4,
    marginHorizontal: '$appMargin',
    height: 40,
    marginBottom: 12,
  },
  textInput: {
    flex: 1,
  },
  icon: {
    marginHorizontal: 10,
  },
  button: {
    marginRight: 12,
  },
  buttonText: {
    fontWeight: 'bold',
  },
});

const SearchBar = ({ onSearch, onChangeText }) => {
  return (
    <View style={styles.container}>
      <Icon style={styles.icon} name="search1" size={18} color="black" />
      <TextInput
        style={styles.textInput}
        placeholderTextColor="#5D5D67"
        clearButtonMode="while-editing"
        defaultValue=""
        onChangeText={onChangeText}
      />
      <TouchableOpacity style={styles.button} onPress={onSearch}>
        <Text style={styles.buttonText}>Search</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SearchBar;
