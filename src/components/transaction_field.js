import dayjs from 'dayjs';
import React from 'react';
import { View, Text } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';

const styles = EStylesheet.create({
  container: {
    flex: 1,
    marginBottom: 8,
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 3,
  },
  text: {
    width: '90%',
  },
});

const TransactionField = ({ name, value }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{`${name}: `}</Text>
      <Text style={styles.text}>{value}</Text>
    </View>
  );
};

export default TransactionField;
