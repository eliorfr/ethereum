import { getUniqueId } from 'react-native-device-info';
import { sha512 } from 'js-sha512';

export default () => {
  const deviceId = getUniqueId();
  const hashDeviceId = sha512(deviceId);
  return hashDeviceId;
};
