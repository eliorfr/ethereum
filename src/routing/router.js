import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, DefaultTheme as NavDefaultTheme } from '@react-navigation/native';
import { navigationRef, onStateChange } from '../utils/navigation_service.js';

import Home from '../screens/home.js';
import { useHeaderStyle } from '../hooks/use_header_style.js';
import HeaderTitle from '../components/header_title.js';
import HeaderBackButton from '../components/header_back_button.js';

const navigationTheme = {
  ...NavDefaultTheme,
  colors: {
    ...NavDefaultTheme.colors,
    background: 'white',
  },
};

const Router = () => {
  const Stack = createStackNavigator();
  const { headerStyles } = useHeaderStyle();

  return (
    <NavigationContainer ref={navigationRef} theme={navigationTheme} onStateChange={onStateChange}>
      <Stack.Navigator initialRouteName="MainTabs" screenOptions={{ gestureEnabled: false }}>
        {/* <Stack.Screen name="MainTabs" children={MainTabs} options={{ headerShown: false }} /> */}
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            headerStyle: headerStyles.header,
            headerTitleAlign: 'center',
            headerTitle: props => <HeaderTitle props={props} title="Transactions" />,
            headerLeft: null,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default React.memo(Router);
