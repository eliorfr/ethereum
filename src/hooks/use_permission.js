import { useEffect, useState } from 'react';
import { Platform } from 'react-native';
import { PERMISSIONS, requestMultiple, checkMultiple } from 'react-native-permissions';

const typesMapping = {
  camera: Platform.select({
    android: PERMISSIONS.ANDROID.CAMERA,
    ios: PERMISSIONS.IOS.CAMERA,
  }),
  microphone: Platform.select({
    android: PERMISSIONS.ANDROID.RECORD_AUDIO,
    ios: PERMISSIONS.IOS.MICROPHONE,
  }),
  writeExternalStorage: Platform.select({
    android: PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
    ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
  }),
  readExternalStorage: Platform.select({
    android: PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
    ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
  }),
};

export const requestPermission = async permissionTypes => {
  permissionTypes = permissionTypes.map(p => typesMapping[p]);

  let statuses = await checkMultiple(permissionTypes);
  let unApprovedPermissions = permissionTypes.filter(p => statuses[p] === 'denied');
  if (unApprovedPermissions.length > 0) {
    statuses = await requestMultiple(unApprovedPermissions);
  }
  return Object.values(statuses).every(p => p === 'granted');
};

export const usePermission = permissionTypes => {
  const [permissionsStatus, setPermissionsStatus] = useState(null);

  useEffect(() => {
    const requestAndSetPermissionsStatus = async () => {
      const statuses = await requestPermission(permissionTypes);
      setPermissionsStatus(statuses);
    };
    requestAndSetPermissionsStatus();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return permissionsStatus;
};
