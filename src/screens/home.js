import axios from 'axios';
import React, { useState } from 'react';
import { Alert } from 'react-native';
import { View, Text } from 'react-native';
import EStylesheet from 'react-native-extended-stylesheet';
import { FlatList } from 'react-native-gesture-handler';
import LoadMore from '../components/load_more';
import SearchBar from '../components/search_bar';

import Transaction from '../components/transaction';
import { isValidAddress } from '../utils/validator';

const styles = EStylesheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    padding: 12,
    flexGrow: 1,
  },
});

const Home = () => {
  const [transactions, setTransactions] = useState([]);
  const [term, setTerm] = useState('');
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const clearData = () => {
    setTransactions([]);
    setPage(1);
  };

  const getData = async () => {
    if (!isValidAddress(term)) {
      Alert.alert('Address is invalid.');
      return;
    }
    try {
      setLoading(true);
      const apiKey = 'BP5YUJW41DB5QGHK5RAA7YAAET4AKFKSUG';
      const baseUrl = 'https://api.etherscan.io/api';
      const url = `${baseUrl}?module=account&action=txlist&address=${term}&startblock=0&endblock=99999999&page=${page}&offset=10&sort=asc&apikey=${apiKey}`;
      const response = await axios.get(url);
      setPage(v => v + 1);
      if (response.data.message !== 'OK') {
        Alert.alert(response.data.message);
        return;
      }
      setTransactions(v => [...v, ...response.data?.result]);
    } catch (error) {
      Alert.alert('Error');
    } finally {
      setLoading(false);
    }
  };

  const onChangeText = async text => {
    setTerm(text);
    if (transactions.length > 0) {
      setTimeout(() => clearData(), 2000);
    }
  };

  return (
    <View style={styles.container}>
      <SearchBar onChangeText={onChangeText} onSearch={getData} />
      <FlatList
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        data={transactions}
        keyExtractor={item => item.hash}
        renderItem={({ item }) => <Transaction transaction={item} />}
        ListFooterComponent={() => <LoadMore getData={getData} loading={loading} />}
      />
    </View>
  );
};

export default Home;
