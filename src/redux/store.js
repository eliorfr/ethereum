import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise';
import freeze from 'redux-freeze';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { persistStore, persistReducer, createTransform } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import reducers from './reducers';
import Loading from '../components/loading';

const logger = createLogger({
  collapsed: true,
});

const reduxMiddlewares = [thunk, promise];
if (__DEV__) {
  reduxMiddlewares.push(freeze);
  reduxMiddlewares.push(logger);
}

const whitelist = ['users'];

const inboundTransformer = (inboundState, key) => {
  switch (key) {
    default:
      return inboundState;
  }
};

const outboundTransformer = (outboundState, key) => {
  switch (key) {
    default:
      return outboundState;
  }
};

const setTransform = createTransform(inboundTransformer, outboundTransformer, { whitelist });

const persistOptions = {
  key: 'root',
  storage: AsyncStorage,
  whitelist,
  timeout: 0,
  transforms: [setTransform],
};

const persistedReducer = persistReducer(persistOptions, reducers);

export const store = createStore(persistedReducer, {}, applyMiddleware(...reduxMiddlewares));
export const persistor = persistStore(store);

export const withReduxProvider = App => {
  return props => {
    return (
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <App {...props} />
        </PersistGate>
      </Provider>
    );
  };
};
