import { combineReducers } from 'redux';

const createReducer =
  (actionType, initialState) =>
  (state = initialState, action) => {
    switch (action.type) {
      case actionType:
        return action.payload;
      default:
        return state;
    }
  };

const reducersDefaultValues = {
  users: [],
};

const reducers = {};
Object.keys(reducersDefaultValues).map(key => {
  reducers[key] = createReducer(key, reducersDefaultValues[key]);
});
const appReducer = combineReducers(reducers);

const rootReducer = (state, action) => {
  if (action.type === 'RESET_STORE') {
    state = {};
  }
  return appReducer(state, action);
};

export default rootReducer;
